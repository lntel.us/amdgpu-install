########
Preamble
########

© 2018 Advanced Micro Devices, Inc.  The information contained herein
is for informational purposes only and is subject to change without
notice. While every precaution has been taken in the preparation
of this document, it may contain technical inaccuracies, omissions
and typographical errors, and AMD is under no obligation to update or
otherwise correct this information. Advanced Micro Devices, Inc. makes no
representations or warranties with respect to the accuracy or completeness
of the contents of this document and assumes no liability of any kind,
including the implied warranties of non-infringement, merchantability or
fitness for particular purposes, with respect to the operation or use of
AMD hardware, software or other products described herein. No license,
including implied or arising by estoppel, to any intellectual property
rights is granted by this document. This notice does not change the terms
and limitations applicable to the purchase or use of AMD's products that
may be set forth in a separate signed agreement between you and AMD.

AMD, the AMD Arrow logo, AMD Radeon, AMD FirePro and combinations thereof
are trademarks of Advanced Micro Devices, Inc.

Linux® is the registered trademark of Linus Torvalds in the U.S. and
other countries.

Vulkan and the Vulkan logo are registered trademarks of the Khronos
Group Inc.

OpenCL and the OpenCL logo are trademarks of Apple Inc. used by permission
by Khronos.

OpenGL® and the oval logo are trademarks or registered trademarks of
Hewlett Packard Enterprise in the United States and/or other countries
worldwide.

